apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: homeassistant
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  project: smarthome
  source:
    repoURL: 'https://gitlab.com/api/v4/projects/45323150/packages/helm/stable'
    targetRevision: 2.4.0
    chart: homeassistant
    plugin:
      name: vault-helm-plugin
      env:
        - name: helm_values
          value: |
            image:
              tag: 2025.2.2
            ingress:
              enabled: true
              host: ha.t3k.in
              className: "nginx"
              annotations:
                cert-manager.io/cluster-issuer: "letsencrypt"
            secrets:
              content:
                wifi_name: t3k.iot
                wifi_pass: <path:kv/data/common/wifi#t3k.iot>
                wifi_fallback_name: EspHome
                wifi_fallback_pass: <path:kv/data/common/wifi#esphome_fallback>
                db_uri: postgresql://homeassistant:<path:kv/data/k8s/postgresql#haPassword>@postgresql.postgresql.svc/homeassistant
            tolerations:
              - key: pod-type
                value: light
            affinity:
              nodeAffinity:
                requiredDuringSchedulingIgnoredDuringExecution:
                  nodeSelectorTerms:
                  - matchExpressions:
                    - key: kubernetes.io/arch
                      operator: In
                      values:
                      - arm64
                    - key: hardware/zigbee
                      operator: In
                      values:
                      - slaesh
            persitantVolume:
              config_size: 10Gi
              local_size: 10Gi
            debug:
              enabled: false
            securityContext:
              privileged: true
            zigbee:
              enabled: true
              mount_path: /dev/serial/by-id/usb-Silicon_Labs_slae.sh_cc2652rb_stick_-_slaesh_s_iot_stuff_00_12_4B_00_21_CC_4D_8E-if00-port0
              device_path: /dev/serial/by-id/usb-Silicon_Labs_slae.sh_cc2652rb_stick_-_slaesh_s_iot_stuff_00_12_4B_00_21_CC_4D_8E-if00-port0
            resources:
              limits:
                memory: 3Gi
                cpu: 3
              requests:
                memory: 1Gi
                cpu: 2
            projected_volumes:
              blueprints:
                mountPath: blueprints
                sources:
                  - configMap:
                      name: ha-config-blueprints
                      items:
                        - key: turn_off_light.yaml
                          path: automation/turn_off_light.yaml
                        - key: schedule.yaml
                          path: automation/schedule.yaml
                        - key: camera_snapshot.yaml
                          path: automation/camera_snapshot.yaml
                        - key: covers.yaml
                          path: automation/covers.yaml
                        - key: lightup_on_motion.yaml
                          path: automation/lightup_on_motion.yaml
              switches:
                mountPath: switches
                sources:
                  - configMap:
                      name: ha-config-switches
                      items:
                        - key: guests.yaml
                          path: guests.yaml
                        - key: wol.yaml
                          path: wol.yaml
              zhaquirks:
                mountPath: zha_quirks
                sources:
                  - configMap:
                      name: ha-config-zhaquirks
                      items:
                        - key: electric_heating.py
                          path: electric_heating.py
                        - key: ts130f.py
                          path: ts130f.py
              ui:
                mountPath: ui
                sources:
                  - configMap:
                      name: ha-config-ui
                      items:
                        - key: systems.yaml
                          path: systems.yaml
                        - key: spoonlab.yaml
                          path: spoonlab.yaml
                        - key: main.yaml
                          path: main.yaml
                  - configMap:
                      name: ha-config-views
                      items:
                      - key: garaz.yaml
                        path: views/garaz.yaml
                      - key: pralnia.yaml
                        path: views/pralnia.yaml
                      - key: kotlownia.yaml
                        path: views/kotlownia.yaml
                      - key: kuchnia.yaml
                        path: views/kuchnia.yaml
                      - key: dolna_lazienka.yaml
                        path: views/dolna_lazienka.yaml
                      - key: julka.yaml
                        path: views/julka.yaml
                      - key: dom.yaml
                        path: views/dom.yaml
                      - key: olek.yaml
                        path: views/olek.yaml
                      - key: sypialnia.yaml
                        path: views/sypialnia.yaml
                      - key: gorna_lazienka.yaml
                        path: views/gorna_lazienka.yaml
                      - key: gabinet.yaml
                        path: views/gabinet.yaml
                      - key: wiatrolap.yaml
                        path: views/wiatrolap.yaml
                      - key: ogrod.yaml
                        path: views/ogrod.yaml
                      - key: salon.yaml
                        path: views/salon.yaml
                      - key: bawialnia.yaml
                        path: views/bawialnia.yaml
                  - configMap:
                      name: ha-config-decluttering
                      items:
                      - key: decluttering_templates.yaml
                        path: decluttering_templates/decluttering_templates.yaml
                      - key: room_cards.yaml
                        path: decluttering_templates/room_cards.yaml
              packages:
                mountPath: packages
                sources:
                  - configMap:
                      name: ha-config-packages
                      items:
                      - key: home.yaml
                        path: home.yaml
                      - key: garaz.yaml
                        path: garaz.yaml
                      - key: poranek.yaml
                        path: poranek.yaml
                      - key: plusgsm.yaml
                        path: plusgsm.yaml
                      - key: mijia1c.yaml
                        path: mijia1c.yaml
                      - key: tv.yaml
                        path: tv.yaml
                      - key: garbage.yaml
                        path: garbage.yaml
                      - key: wieczor.yaml
                        path: wieczor.yaml
                      - key: weather.yaml
                        path: weather.yaml
                      - key: noc.yaml
                        path: noc.yaml
                      - key: dzien.yaml
                        path: dzien.yaml
                      - key: notify_us.yaml
                        path: notify_us.yaml
                      - key: covers.yaml
                        path: covers.yaml
            template_configmaps:
              core-customize:
                cm: ha-config-core
                path: customize.yaml
                key: customize.yaml
              core-recorder:
                cm: ha-config-core
                path: recorder.yaml
                key: recorder.yaml
              core-configuration:
                cm: ha-config-core
                path: configuration.yaml
                key: configuration.yaml
              core-groups:
                cm: ha-config-core
                path: groups.yaml
                key: groups.yaml
              core-device-trackers:
                cm: ha-config-core
                path: device_trackers.yaml
                key: device_trackers.yaml
  destination:
    server: {{ .Values.spec.destination.server }}
    namespace: smarthome
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
    - CreateNamespace=false
